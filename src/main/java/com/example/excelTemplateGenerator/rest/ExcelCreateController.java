package com.example.excelTemplateGenerator.rest;

import com.example.excelTemplateGenerator.entity.ExcelEntity;
import com.example.excelTemplateGenerator.entity.ExcelFields;
import com.example.excelTemplateGenerator.service.ExcelCreateService;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileOutputStream;
import java.io.IOException;


@RestController
@RequestMapping("/excel")
public class ExcelCreateController {

    @Autowired
    private ExcelCreateService excelCreateService;
    @GetMapping("/createexcel")
    public ResponseEntity<?> createExcel(@RequestBody ExcelEntity excelEntity) throws IOException {
        return excelCreateService.createExcelDropDown(excelEntity);

    }




}
