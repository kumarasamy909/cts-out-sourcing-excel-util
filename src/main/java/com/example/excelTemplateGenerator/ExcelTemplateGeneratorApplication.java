package com.example.excelTemplateGenerator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExcelTemplateGeneratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExcelTemplateGeneratorApplication.class, args);
	}

}
