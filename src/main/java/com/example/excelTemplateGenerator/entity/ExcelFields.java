package com.example.excelTemplateGenerator.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data

public class ExcelFields {
  private String heading;

  private String[] dropdown ;

}
