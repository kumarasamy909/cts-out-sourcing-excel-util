package com.example.excelTemplateGenerator.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;


@Data
public class ExcelEntity {

    private String templateName;
    private List<ExcelFields>  excelFields;


}
