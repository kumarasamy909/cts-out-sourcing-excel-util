package com.example.excelTemplateGenerator.service;

import com.example.excelTemplateGenerator.entity.ExcelEntity;
import org.springframework.http.ResponseEntity;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface ExcelCreateService {
    ResponseEntity<?> createExcelDropDown(ExcelEntity excelEntity) throws IOException;
}
