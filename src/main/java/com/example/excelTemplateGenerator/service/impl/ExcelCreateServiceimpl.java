package com.example.excelTemplateGenerator.service.impl;

import com.example.excelTemplateGenerator.entity.ExcelEntity;
import com.example.excelTemplateGenerator.entity.ExcelFields;
import com.example.excelTemplateGenerator.service.ExcelCreateService;
import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;

@Service
public class ExcelCreateServiceimpl implements ExcelCreateService {

    @Override
    public ResponseEntity<?> createExcelDropDown(ExcelEntity excelEntity) throws IOException {

        HSSFWorkbook workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet(excelEntity.getTemplateName());
        int lastRow = workbook.getSpreadsheetVersion().getLastRowIndex();
        Row row = sheet.createRow(0);
        int datarowIndex = 0;

        for (ExcelFields findindex : excelEntity.getExcelFields()) {
            row.createCell(datarowIndex).setCellValue(findindex.getHeading());
            if(findindex.getDropdown()!=null){
                sheet =     createExceldropdown(findindex.getDropdown(),findindex.getHeading(),datarowIndex,sheet,row,lastRow);
            }
            datarowIndex++;
        }
        FileOutputStream fout = new FileOutputStream("F:\\ExcelFolder\\mypoi.xls");
        workbook.write(fout);
        workbook.close();

        return   ResponseEntity.ok("Excel File Created");
    }

    private Sheet createExceldropdown(String[] dropdown, String heading, int datarowIndex, Sheet sheet, Row row, int lastRow) {

        row.createCell(datarowIndex).setCellValue(heading);
        CellRangeAddressList addressList = new CellRangeAddressList(1, lastRow, datarowIndex, datarowIndex);
        DVConstraint dvConstraint = DVConstraint
                .createExplicitListConstraint(dropdown);
        DataValidation dataValidation = new HSSFDataValidation(addressList,
                dvConstraint);
        dataValidation.setSuppressDropDownArrow(false);
        sheet.addValidationData(dataValidation);

        return sheet;

    }
}
